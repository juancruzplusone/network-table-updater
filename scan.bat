@echo off
setlocal EnableDelayedExpansion

:: User specified variables
:: ------------------------------------------
set "outputFile=C:\SomeFolder\ipaddresses.txt"
::Set first 3 octets of IP address
:: Format: xxx.xxx.xxx.
set "ip=192.168.1."
::----------------------------------------
:: Check if outputFile is writable
echo Checking if output file is writable...
(type nul >> "%outputFile%") 2>nul
if not %errorlevel% == 0 (
    echo Error: Cannot write to %outputFile%. Please check permissions or file path.
    exit /b 1
)

:: Clearing the output file
type nul > "%outputFile%"

echo Starting IP scan...
echo This may take some time. Press Ctrl+C to interrupt.

:: Setting up a trap for Ctrl+C (keyboard interrupt)
set "interrupt=0"

for /l %%i in (1,1,254) do (
    if !interrupt! equ 1 (
        echo Interrupted by user.
        goto :end
    )

    echo Checking !ip!%%i...
    ping -n 1 -w 100 !ip!%%i | find "Reply" > nul
    if not errorlevel 1 (
        echo !ip!%%i is up.
        echo !ip!%%i is up. >> "%outputFile%"
    )
)

:end
echo Scan completed. Results have been saved to %outputFile%.

:: Handling Ctrl+C
:CtrlC
set "interrupt=1"
exit /b 0
